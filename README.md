# Interference source localization 

This tutorial will show a POWDER user how to locate an unknown interference signal source.  We will use multiple time-synchronized received signals, recorded at multiple receiver nodes. We will use cross-correlation between pairs of these received signals to identify time difference of arrival (TDOA) measurements, and then localize the source using the combination of the TDOA measurements.

We will use the CBRS rooftop nodes, which are time-synchronized, and can overhear significant signal sources in the CBRS band. For this experiment, it is assumed that you have already have:
- created a POWDER account and have access to node reservations
- the ability to run Python on your local computer.  We use the [Anaconda distribution](https://www.anaconda.com/products/distribution).

## Download files

We will need the following files to implement this tutorial, make sure to download them to the local computer:

- sp.py
- fabfile.py
- interference_loc.ipynb

NOTE: A previous version of this tutorial required a separate 'hostfile.txt', but this is no longer needed.

## Groups for Tutorial


Group A:
- cbrssdr1-hospital
- cbrssdr1-smt
- cbrssdr1-ustar

Group B:
- cbrssdr1-bes
- cbrssdr1-honors
- cbrssdr1-browning
- cbrssdr1-fm


## Instantiate experiment

Once logged onto POWDER, navigate to:

**Experiments &rarr; Start Experiment &rarr; Change Profile &rarr; cir\_localization_update_mww2023 &rarr; Select Profile &rarr; Next**

Now select the compute node type (d740) and base stations for the experiment.  This experiment will not transmit -- we will record ambient signals already on the air, but if you want to use this profile to transmit from node to create an (artificial) "interference" source, you may do that by requesting a frequency range in the CBRS band.

Any of the X310 CBRS Radio can be used. To add a radio, click the plus arrow and select a [location](https://powderwireless.net/area) from the drop down.  Some things to consider about which rooftop nodes to reserve:
 - A minimum of 3 radios is needed for TDoA localization. 
 - Check the [resource availibilty](https://www.powderwireless.net/resinfo.php?embedded=true) to find open radios or reserve them beforehand. 
 - Having nodes only in a straight line is problematic and should be avoided.

**&rarr; Next**

Now you can name your experiment, please identify yourself so that others can see what you're doing.

**&rarr; Next**

Create a start and end time for your experiment (optional). Leaving the fields empty keeps your experiment availible for 16 hours.

**&rarr; Finish**

## SSH into all the nodes

After your experiment is done setting up, SSH into the nodes.

## Upload the sp.py command script to all the nodes

From your local terminal use the following command:

(For Mac users) 

scp [file directory] [node link]:

  `scp /Users/sp.py joy@pc13-fort.emulab.net:`

(For Windows users) 

  `"C:\Program Files (x86)\PuTTY\pscp.exe" -scp [file directory] [node link]:`

for example,

  `"C:\Program Files (x86)\PuTTY\pscp.exe" -scp C:\Users/sp.py joy@pc13-fort.emulab.net:`

## Get access to the command script on all the nodes

On the nodes, use the following command:

`ls` (If the file was successfully scp-ed, the filename will be listed out)

`sudo mv sp.py /usr/bin/` (Move the file to /usr/bin/)

`sudo chmod +x /usr/bin/sp.py` (Enable permission to execute file)

`sudo sysctl -w net.core.wmem_max=24862979` (Resize buffers)

## Signal data collection 

In order to collect data on all the nodes simultaneously, we use fabfile and hostsfile to control all the nodes from the local terminal.

You need the `fabric` package installed on your local machine's python distribution. It can be installed using:

`pip install fabric` 

This will install the latest version.  We assume here fabric version 2.

<!--In hostsfile.txt, type in the nodes that you wish to receive the signal in the following form:

[name of the node] | RX | [link of the node]

honors | RX | ssh -p 22 joy@pc13-fort.emulab.net

hospital | RX | ssh -p 22 joy@pc15-fort.emulab.net

...and so on...-->

In fabfile.py,


<!--(assume password to be POWDER)


1. Change the ssh password to yours in the command line 

   `env.password = 'POWDER'`

2. In def grab_cir_measurements(rx_node_dict):, change the password to yours in the command line 
   
   `result = Connection(host,connect_kwargs={"password":"POWDER"}).get("disturb")`-->
1. Specify the avialable CBRS rooftop nodes in the `host_dict` by putting the names (as keys) and hostname or IP address (as values). Example:

	`host_dict = {'hospital': "joy@pc780.emulab.net", 'smt': "joy@pc772.emulab.net",  'ustar': "joy@pc779.emulab.net"}`

3. Specify the parameters you want to use in RX FUNCTIONS.

   In 
   
    `def rx(rx_min):`
        
        `run("sp.py -f 3580e6 -s 200e6 -lo 100e6 -g 27 -d 0.1 -t %d" % rx_min)`

`-f`  : center frequency

`-s`  : sampling rate

`-lo` : lo offset (usually set to be slightly higher than half of sampling rate)

`-g`  : gain of the receiver

`-d`  : duration time of collection (in second)

* In the local terminal, use the following command: 
  `fab main`
* The collected signal files will be downloaded to the same directory as the location of `fabfile.py`.


<!--## Transfer measurement files from all the nodes to local
We will transfer the file named 'disturb' from each node in the experiment to the local machine and store each measurement by using the naming convention `rx_{name of the node}_raw`  
From your local terminal use the following command for each node:

`scp joy@pc13-fort.emulab.net:disturb ./rx_honors_raw` 
`scp joy@pc15-fort.emulab.net:disturb ./rx_hospital_raw` 

and so on..-->

Compress the directories created that hold the data files.  There should be one directory for each receiver.  For some reason we are not aware of, the data file in each directory is called `disturb` with no extension.  Select all of these directories and compress them with zip to create one zip file.

## Analysis

With these data files, we can do the cross-correlation and localization operations in post-processing in a python script.  We prefer a python notebook, because the algorithm at this point requires human intervention at multiple steps to make sure that steps are working correctly, and to fix things that don't look correct.  While this process could be automated, it is not currently.

At this point, go to the [Google Colab interference localization notebook](https://colab.research.google.com/drive/1RzH4Wp0ccG2EBEKpYl0KoN1Eg8xehKU_?usp=sharing) and follow the instructions there.


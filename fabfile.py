#!/usr/bin/env python


# Works for Fabric version 2


from fabric import Connection, Config 
from fabric.tasks import task
import datetime


from fabric import ThreadingGroup 

# host_dict = {'honors': "aarti@pc13-fort.emulab.net", 'fm': "aarti@pc16-fort.emulab.net"}
host_dict = {'hospital': "aarti@pc702.emulab.net", 'smt': "aarti@pc703.emulab.net",  'ustar': "aarti@pc704.emulab.net"}

host_list = list(host_dict.values())

    
@task
def main(g):
    threads = ThreadingGroup(*host_list)
    dtm = datetime.datetime.now().minute + 1
    threads.run("sp.py -f 3580M -s 20M -lo 12M -g 27 -d 0.1 -t %d" % dtm)
    threads.get("disturb")





